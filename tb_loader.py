'''
Created on 2017. 3. 23.

@author: jaeminson
'''
import os
from sets import Set
from subprocess import Popen, PIPE
import subprocess
import sys
import matplotlib.pyplot as plt

servername="server10"
summary_dir="kaggle_DR/summary/"

ip_addresses={"server10":"210.116.109.42"}

def find_port(instr):
    ports=[]
    s_index=instr.find(":")
    while s_index!=-1:
        dlen=1
        for i in xrange(1,10):
            try:
                int(instr[s_index+i]) 
                dlen+=1
            except:
                break
        if dlen>1:
            port=int(instr[s_index+1:s_index+dlen])
            if port not in ports:
                ports.append(port)
        instr=instr[s_index+dlen:]
        s_index=instr.find(":")
    return ports

# show files under the summary folder
print "*****    summary files on the server    *****"
subprocess.call("ssh "+servername+ " \"ls kaggle_DR/summary\"",shell=True)

# wait for the user's response
print "*****    type in summary file    *****"
summary_fn=sys.stdin.readline()

# get train/test loss/accuracy
loss_acc_file=os.path.join(summary_dir,summary_fn.rstrip("\n"),"training_validation_loss_accuracy.txt") 
ssh = Popen(['ssh', 'server10', 'cat', loss_acc_file], stdout=subprocess.PIPE)
train_losses,test_losses,train_accs,test_accs=[],[],[],[]
for line in ssh.stdout:
    train_loss,train_acc,test_loss,test_acc=line.rstrip("\n").split(",")
    train_losses.append(train_loss)
    test_losses.append(test_loss)
    train_accs.append(train_acc)
    test_accs.append(test_acc)

# draw graphs
t=range(1,1000*len(train_losses),1000)
plt.figure(1)
plt.subplot(211)
plt.plot(t, train_losses, 'b-',label="training")
plt.plot(t, test_losses, 'r-',label="testing")
plt.legend(bbox_to_anchor=(0.75, 0.95), loc=2, borderaxespad=0.)
plt.xlabel('iterations')
plt.ylabel('loss')
axes = plt.gca()
axes.set_ylim([0,1.])

plt.subplot(212)
plt.plot(t, train_accs, 'b-', label="training")
plt.plot(t, test_accs, 'r-',label="testing")
plt.legend(bbox_to_anchor=(0.75, 0.3), loc=2, borderaxespad=0.)
plt.xlabel('iterations')
plt.ylabel('accuracy')
plt.show(block=False)

# find an empty port on the remote server and get an available port 
p = Popen(['ssh',servername,'lsof', '-Pn', '-i4', '-F', 'n'], stdin=PIPE, stdout=PIPE, stderr=PIPE)
output, err = p.communicate()
server_occupied_ports=find_port(output)
exitcode = p.returncode
server_avail_port=-1
occupied_ports=Set(server_occupied_ports)
for cand in xrange(9000,10000):
    if cand not in occupied_ports:
        server_avail_port=cand
        break

# open tensorboard on the server
print ("connect to "+str(ip_addresses[servername])+":"+str(server_avail_port))
subprocess.call("ssh "+servername+" tensorboard --port="+str(server_avail_port)+" --logdir="+os.path.join(summary_dir,summary_fn),shell=True)

